# Defined interactively
function dltime --description 'Youtube-dl a video, saving to filename, stating at timestamp, ending after time'

ffmpeg -y -ss $argv[3] -i (yt-dlp -f 18 -g "$argv[1]") -t $argv[4] $argv[2].mp4 && mpv $argv[2].mp4
end
