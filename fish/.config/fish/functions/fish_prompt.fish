function fish_prompt --description 'Write out the prompt'
  set -l exit_status $status

  set -l home_escaped (echo -n $HOME | sed 's/\//\\\\\//g')
  set -l pwd (echo -n $PWD | sed "s/^$home_escaped/~/")

  set -l prompt_exit_status ''
  if [ $exit_status != 0 ]
    set prompt_exit_status (set_color red)"<Exit Status: $exit_status>"(set_color normal)"\n"
  end

  set -l user_color normal
  set -l prompt_symbol ''
  switch "$USER"
    case root toor
      set prompt_symbol '#'
      set user_color red
    case '*'
      set prompt_symbol '$'
      set user_color magenta
  end
  set -l user_prompt (set_color $user_color)"$USER"(set_color normal)

  set -l hostname_prompt (set_color blue)(hostname)(set_color normal)
  set -l dir_prompt (set_color cyan)"$pwd"(set_color normal)

  printf "%b[%s@%s %s]%s " $prompt_exit_status $user_prompt   $hostname_prompt $dir_prompt $prompt_symbol
end
