function agit
    # run git parameters on all git subdirectories (e.g agit pull)
    find * -maxdepth 1 -type d -name .git -exec sh -c "cd \"{}\"/../ && pwd && git "$argv" && echo" \;
end
