# If not running interactively, don't do anything
[[ $- != *i* ]] && return

PROMPT_COMMAND=__prompt_command
function __prompt_command () {
    local EXIT="$?"

    # terminal colors
    local red='\[\033[38;5;1m\]'
    local orange='\[\033[38;5;3m\]'
    local blue='\[\033[38;5;4m\]'
    local magenta='\[\033[38;5;5m\]'
    local cyan='\[\033[38;5;6m\]'
    local normal='\[\033[38;5;15m\]'

    PS2="${orange}> ${normal}"
    PS1=""

    if [[ $EXIT != 0 ]]; then
        PS1+="${red}<Exit Status: ${EXIT}>${normal}\n"
    fi

    PS1+="["

    if [[ $USER == root ]]; then
        PS1+="${red}"
    else
        PS1+="${magenta}"
    fi

    # [username@hostname $PWD]$
    PS1+="\u${normal}@${blue}\H ${cyan}\w${normal}]\\$ "
}

# Alias definitions.
# You may want to put all your additions into a separate file like
# ~/.bash_aliases, instead of adding them here directly.
# See /usr/share/doc/bash-doc/examples in the bash-doc package.

if [ -f ~/.bash_aliases ]; then
    . ~/.bash_aliases
fi
