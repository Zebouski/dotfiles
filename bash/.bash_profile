#
# ~/.bash_profile
#

# Vesta Control Panel
if [[ -d /usr/local/vesta/ ]]; then
  PATH=$PATH:/usr/local/vesta/bin
fi

[[ -f ~/.bashrc ]] && . ~/.bashrc

export PATH
